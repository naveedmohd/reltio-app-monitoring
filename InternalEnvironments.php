<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Internal Environments</title>
</head>

<style>
    table {
        border-collapse: collapse;
        width: 70%;
    }

    th{
        text-align: left;
        padding: 10px;
    }

    td {
        text-align: center;
        padding: 2px;
    }

    tr:nth-child(even){background-color: lightyellow}

    th {
        background-color: #4CAF50;
        color: white;
    }

    .styled-select select {
        background: transparent;
        width: 400px;
        padding: 5px;
        font-size: 16px;
        line-height: 1;
        border: 1;
        border-radius: 0;
        height: 30px;
        margin-left: 100px;
        margin-top: 0px;
        scrollbar-highlight-color: #CC0000;
        background-color: floralwhite;
        align-content: center;
        -webkit-appearance: none;
    }
</style>

<body>
<?php
include ('SideBarNavigation.php');
require('GlobalAccessToken.php');
require('TenantStatisticsByEnvironment.php');
require ('ReadSQSQueueMessages.php');
$envName="";
$authToken="";
?>


<?php

    $dbhost = 'localhost:3306';
    $dbuser = 'root';
    $dbpass = 'Admin$786';
    $dbname='monitoring';

    try {
        $conn = new PDO("mysql:host=$dbhost;dbname=monitoring", $dbuser, $dbpass);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Connected successfully";
    }
    catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();
    }
?>


<?php
if(isset($_GET["environment"])){
    $envName=$_GET["environment"];
    //echo "select environment is => ".$envName;
}
?>

<table id="dataTable" align="center" style="margin-top:50px; margin-left:300px;" border="0">
    <form id="InternalEnvironments">
    <tr align="center">
        <td align="center">
            <div class="styled-select">
            <select name="environment"  onchange="this.form.submit()">
                <option selected="selected" >Choose an environment from this list</option>
                <?php

                $environments = array("idev-01-dev.reltio.com"
                , "idev-02.reltio.com"
                , "tst-01.reltio.com"
                , "tst-01-hf.reltio.com"
                , "test-03.reltio.com"
                , "gtst-01-hf.reltio.com"
                , "gtst-02.reltio.com"
                , "geu-tst-01.reltio.com"
                , "tst-04-hf.reltio.com"
                , "eutst-01.reltio.com"
                , "pm.reltio.com"
                , "rasp-01.reltio.com"
                , "geu-test.reltio.com"
                , "perf-03-dataload.reltio.com"
                , "perf-05-dataload.reltio.com"
                , "perf-06.reltio.com"
                , "perf-07.reltio.com"
                , "perf-08.reltio.com"
                , "perf-09-auth.reltio.com"
                , "perf-usg-api.reltio.com"
                );
                // Iterating through the environment array
                $selected = isset($_REQUEST['environment']) ? $_REQUEST['environment'] : '';
                foreach($environments as $item){
                    $item = htmlspecialchars($item);
                    ?>
                    <?php
                    echo '<option value="'. $item .'"'.(($item==$selected)?' selected':'').'>'. $item .'</option>';
                }
                ?>
            </select>
            </div>
        </td>

    </tr>
        </form>


</table>
<tr></tr>
<tr></tr>



<table id="EntityStatistics" align="left"  border="1" style="margin-top:50px; margin-left:300px; background-color: #F0FFFF" >




    <tr align="center" bgcolor="#00ffff" >
        <td ><b>TenantId</b></td>
        <td ><b>Entity Count</b></td>
        <td ><b>SQS CRUD - MATCH Queue Name</b></td>
        <td ><b>Internal CRUD Events</b></td>
        <td ><b>Internal MATCH Events</b></td>
    </tr>


    <!-- Get AuthToken -->
    <?php




    if ($envName != null && $authToken==null) {
        $authResponse = getAuthToken($envName);
        //echo "Here is response code:".$authResponse;
        $authToken = json_decode($authResponse);
        $authToken = $authToken->{'access_token'};
        //echo $authToken;


        //Get List of AllTenantIDs for a given environment.
        try{
            $response = getTenantStatistics($envName, $authToken);
            //echo $response;
        }catch(Exception $e){
            if (substr_count($e->getMessage(), 'invalid') > 0) {
                $authResponse = getAuthToken($envName);
                $authToken = json_decode($authResponse);
                $authToken = $authToken->{'access_token'};
            }
        }

        $resArr = array();
        $resArr = json_decode($response);
        //echo $resArr;


        //DELETE data from table for selected environment name
        try{
            $sql_stmt="DELETE FROM ENV_QUEUE_MONITORING_INT WHERE ENVIRONMENT_NAME='". $envName ."'";
            echo $sql_stmt;
            $conn->exec($sql_stmt);
        }catch(PDOException $e){
            echo $sql_stmt . "<br>" . $e->getMessage();
        }

        foreach ($resArr as $key=>$tenantId){
        ?>


            <tr align="center" >
                <td>
                    <?php echo '<pre>';print_r("$tenantId <br>"); echo '</pre>';?>

                </td>


            <?php
                $entityCount = getEntityCountForTenant($envName,$authToken,$tenantId);
                //echo $entityCount
            ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$entityCount <br>");
                    echo '</pre>'; ?>

                </td>

                <?php
                $crudqueuename="sqs-crud-".substr($envName,0,-11)."_".$tenantId;
                ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$crudqueuename <br>");
                    echo '</pre>'; ?>
                    <?php
                    $matchqueuename="sqs-match-".substr($envName,0,-11)."_".$tenantId;
                    echo '<pre>';
                    print_r("$matchqueuename <br>");
                    echo '</pre>';
                    ?>

                </td>

                <?php
                $crudqueuecount=send_to_sqs($crudqueuename);
                ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$crudqueuecount <br>");
                    echo '</pre>'; ?>

                </td>



                <?php
                $matchqueuecount=send_to_sqs($matchqueuename);
                ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$matchqueuecount <br>");
                    echo '</pre>'; ?>

                </td>

            </tr>

    <?php

            try {
                $sql_stmt = "INSERT INTO ENV_QUEUE_MONITORING_INT (
                                    ENVIRONMENT_NAME
                                    ,TENANT_ID
                                    ,ENTITY_COUNT
                                    ,SQS_CRUD_QUEUE_NAME
                                    ,SQS_MATCH_QUEUE_NAME
                                    ,CRUD_EVENTS,MATCH_EVENTS)
                            VALUES ('" . $envName . "'
                                        ,'" . $tenantId . "'
                                        , '" . $entityCount . "'
                                        , '" . $crudqueuename . "'
                                        , '" . $matchqueuename . "'
                                        , '" . $crudqueuecount . "'
                                        , '" . $matchqueuecount
                                        . "')";
                echo($sql_stmt);
                $conn->exec($sql_stmt);
            }catch(PDOException $e){
                //echo $sql_stmt . "<br>" . $e->getMessage();
            }
        }
    }
    ?>

</table>


<?php include('Footer.php'); ?>
</body>
</html>