<?php
require '/Applications/XAMPP/vendor/autoload.php';

use Aws\Sqs\SqsClient;

//A function to send message to SQS queue
function send_to_sqs($queueName){
    $configs = include('readConfig.php');
    //$AWS_KEY="AKIAJMCSGFYQ2365SMTQ";
    //$AWS_SECRET="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";
    //$queueUrl="https://sqs.us-east-1.amazonaws.com/930358522410/tst-01-hf_wlmspinsdevtst01hf";

    $AWS_KEY=$configs->INT_AWS_KEY;
    $AWS_SECRET=$configs->INT_AWS_SECRET;

    //$queueUrl="https://sqs.us-east-1.amazonaws.com/930358522410/sqs-crud-tst-01-hf_wlmspinsdevtst01hf";

//Connect to SQS
    $client = SqsClient::factory(array(
        'credentials' => array (
            'key' => $AWS_KEY, //use your AWS key here
            'secret' => $AWS_SECRET //use your AWS secret here
        ),

        'region' => 'us-east-1', //replace it with your region
        'version' => 'latest'
    ));

    try {
        $queueUrl = $client->getQueueUrl(array(
            'QueueName' => $queueName
        ));
    }catch(Exception $e){
        //echo 'Message: ' . $e->getMessage();
        if (substr_count($e->getMessage(), 'NonExistentQueue') > 0) {
            $queueUrl="QUEUE_URL_NOT_FOUND";
            //echo "What is Queue url now:".$queueUrl;
        }
    }

    if (substr_count($queueUrl, 'QUEUE_URL_NOT_FOUND')>0) {
        $queueUrl="QUEUE_URL_NOT_FOUND";
    }else{
        try {
            $queueUrl = $queueUrl->get('QueueUrl');
        } catch (Exception $e) {
            //echo 'Message: ' . $e->getMessage();
            //$e = json_decode($e);
            if (substr_count($e->getMessage(), 'NonExistentQueue') > 0) {
                $queueUrl = "Queue URL does not exist for the tenant";
            }
        }
    }
    //echo $queueUrl;

    if (substr_count($queueUrl, 'QUEUE_URL_NOT_FOUND')>0){
        return $result="SQS Queue does not exist.";
    }else {
        $result = $client->getQueueAttributes(
            ['QueueUrl' => $queueUrl
                , 'AttributeNames' => ['ApproximateNumberOfMessages'
                , 'ApproximateNumberOfMessagesNotVisible'
                , 'ApproximateNumberOfMessagesDelayed']]
        );
        $result = $result->get('Attributes');
        if (!empty($result['ApproximateNumberOfMessages']) && $result['ApproximateNumberOfMessages'] > 0) {
            echo "Messages:" . $result['ApproximateNumberOfMessages'];
            return $result['ApproximateNumberOfMessages'];
        } else {
            return $result = "There are 0 Messages.";
        }
    }



}

?>