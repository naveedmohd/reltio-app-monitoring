<?php
require '/Applications/XAMPP/vendor/autoload.php';

use Aws\Sqs\SqsClient;

//A function to send message to SQS queue
function send_to_sqs($queueName,$awsRegion,$queuePrefix){
    $configs = include('readConfig.php');


    if($queuePrefix=='rp-07') {
        $AWS_KEY = $configs->PREVIEW_AWS_KEY;
        $AWS_SECRET = $configs->PREVIEW_SECRET_KEY;
    }else{
        $AWS_KEY = $configs->EXT_AWS_KEY;
        $AWS_SECRET = $configs->EXT_AWS_SECRET;
    }

    $AWS_REGION=$configs->$awsRegion;


//Connect to SQS
    $client = SqsClient::factory(array(
        'credentials' => array (
            'key' => $AWS_KEY, //use your AWS key here
            'secret' => $AWS_SECRET //use your AWS secret here
        ),

        'region' => $AWS_REGION, //replace it with your region
        'version' => 'latest'
    ));

    try {
        $queueUrl = $client->getQueueUrl(array(
            'QueueName' => $queueName
        ));
    }catch(Exception $e){
        //echo 'Message: ' . $e->getMessage();
        if (substr_count($e->getMessage(), 'NonExistentQueue') > 0) {
            //echo $e->getMessage();
            $queueUrl="QUEUE_URL_NOT_FOUND";
            //echo "What is Queue url now:".$queueUrl;
        }
    }
    //echo "Waht is url:".$queueUrl;

    if (substr_count($queueUrl, 'QUEUE_URL_NOT_FOUND')>0) {
        $queueUrl="QUEUE_URL_NOT_FOUND";
    }else{
        try {
            $queueUrl = $queueUrl->get('QueueUrl');
            //echo $queueUrl;
        } catch (Exception $e) {
            //echo 'Message: ' . $e->getMessage();
            //$e = json_decode($e);
            if (substr_count($e->getMessage(), 'NonExistentQueue') > 0) {
                $queueUrl = "Queue URL does not exist for the tenant";
            }
        }
    }
    //echo $queueUrl;

    if (substr_count($queueUrl, 'QUEUE_URL_NOT_FOUND')>0){
        return $result="Messages could not be retrieved.";
    }else {
        $result = $client->getQueueAttributes(
            ['QueueUrl' => $queueUrl
                , 'AttributeNames' => ['ApproximateNumberOfMessages'
                , 'ApproximateNumberOfMessagesNotVisible'
                , 'ApproximateNumberOfMessagesDelayed']]
        );
        $result = $result->get('Attributes');
        if (!empty($result['ApproximateNumberOfMessages']) && $result['ApproximateNumberOfMessages'] > 0) {
            echo "Messages:" . $result['ApproximateNumberOfMessages'];
            return $result['ApproximateNumberOfMessages'];
        } else {
            return $result = "There are 0 Messages.";
        }
    }



}

?>