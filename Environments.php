<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Customer Facing Environments</title>
</head>

<style>
    table {
        border-collapse: collapse;
        width: 70%;
    }

    th{
        text-align: left;
        padding: 10px;
    }

    td {
        text-align: center;
        padding: 2px;
    }

    tr:nth-child(even){background-color: lightyellow}

    th {
        background-color: #4CAF50;
        color: white;
    }

    .styled-select select {
        background: transparent;
        width: 400px;
        padding: 5px;
        font-size: 16px;
        line-height: 1;
        border: 1;
        border-radius: 0;
        height: 30px;
        margin-left: 100px;
        margin-top: 0px;
        scrollbar-highlight-color: #CC0000;
        background-color: floralwhite;
        align-content: center;
        -webkit-appearance: none;
    }
</style>

<body>
<?php
include ('SideBarNavigation.php');
require('GlobalAccessTokenExt.php');
require('TenantStatisticsByEnvironmentExt.php');
require ('ReadSQSQueueMessagesExt.php');
$envName="";
$authToken="";
$crud_match_QueuePrefix="";
$matchQueuePrefix="";
?>


<?php
    function getQueuePrefix($environmentName){
        $configs = include('readConfig.php');
        $crud_match_QueuePrefix=$configs->$environmentName;

        return $crud_match_QueuePrefix;
    }
?>

<?php
    function getAWSRegion($environmentName){
        $configs = include('readConfig.php');
        //Get AWS Region Names
        if(substr($environmentName,0,-11)=="mpe-01"){
            $awsRegion="AWS_EU_REGION";
        }elseif (substr($environmentName,0,-11)=="euprod-01"){
            $awsRegion="AWS_EU_REGION";
        }elseif (substr($environmentName,0,-11)=="mpe-02"){
            $awsRegion="AWS_APAC_REGION";
        }elseif (substr($environmentName,0,-11)=="approd-01"){
            $awsRegion="AWS_APAC_REGION";
        }elseif (substr($environmentName,0,-11)=="rp-07") {
            $awsRegion = "AWS_US_EAST_REGION";
        }else{
            $awsRegion="AWS_US_EAST_REGION";
        }

        return $awsRegion;
    }
?>

<?php
if(isset($_GET["environment"])){
    $envName=$_GET["environment"];
    //echo "select environment is => ".$envName;
}
?>

<table id="dataTable" align="center" style="margin-top:50px; margin-left:300px;" border="0">
    <form id="InternalEnvironments">
        <tr align="center">
            <td align="center">
                <div class="styled-select">
                    <select name="environment"  onchange="this.form.submit()">
                        <option selected="selected" >Choose an environment from this list</option>
                        <?php

                        $environments = array(
                        "pilot.reltio.com"
                        , "sndbx.reltio.com"
                        , "prod-h360.reltio.com"
                        , "test-h360.reltio.com"
                        , "dev-h360.reltio.com"
                        , "test.reltio.com"
                        , "test-usg.reltio.com"
                        , "dev.reltio.com"
                        , "361.reltio.com"
                        , "prod-usg.reltio.com"
                        , "eu-dev.reltio.com"
                        , "eu-test.reltio.com"
                        , "geu-dev.reltio.com"
                        , "geu-test.reltio.com"
                        , "eu-360.reltio.com"
                        , "geu-360.reltio.com"
                        , "ap-dev.reltio.com"
                        , "ap-test.reltio.com"
                        , "ap-360.reltio.com"
                        , "360p1.reltio.com"
                        , "mpe-03.reltio.com"
                        , "preview.reltio.com"
                        , "pre-01.reltio.com"
                        , "pre-usg.reltio.com"
                        );
                        // Iterating through the environment array
                        $selected = isset($_REQUEST['environment']) ? $_REQUEST['environment'] : '';
                        foreach($environments as $item){
                            $item = htmlspecialchars($item);
                            ?>
                            <?php
                            echo '<option value="'. $item .'"'.(($item==$selected)?' selected':'').'>'. $item .'</option>';
                        }
                        ?>
                    </select>
                </div>
            </td>

        </tr>
    </form>


</table>
<tr></tr>
<tr></tr>



<table id="EntityStatistics" align="left"  border="1" style="margin-top:50px; margin-left:300px; background-color: #F0FFFF" >




    <tr align="center" bgcolor="#00ffff" >
        <td ><b>TenantId</b></td>
        <td ><b>Entity Count</b></td>
        <td ><b>SQS CRUD - MATCH Queue Name</b></td>
        <td ><b>Internal CRUD Events</b></td>
        <td ><b>Internal MATCH Events</b></td>
    </tr>


    <!-- Get AuthToken -->
    <?php

    if ($envName != null && $authToken==null) {
        $authResponse = getAuthToken($envName);
        //echo "Here is response code:".$authResponse;
        $authToken = json_decode($authResponse);
        $authToken = $authToken->{'access_token'};
        //echo $authToken;


        //Get List of AllTenantIDs for a given environment.
        try{
            $response = getTenantStatistics($envName, $authToken);
            //echo $response;
        }catch(Exception $e){
            if (substr_count($e->getMessage(), 'invalid') > 0) {
                $authResponse = getAuthToken($envName);
                $authToken = json_decode($authResponse);
                $authToken = $authToken->{'access_token'};
            }
        }

        $resArr = array();
        $resArr = json_decode($response);
        //echo $resArr;



        foreach ($resArr as $key=>$tenantId){
            ?>


            <tr align="center" >
                <td>
                    <?php echo '<pre>';print_r("$tenantId <br>"); echo '</pre>';?>

                </td>


                <?php
                $entityCount = getEntityCountForTenant($envName,$authToken,$tenantId);
                //echo $entityCount
                ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$entityCount <br>");
                    echo '</pre>'; ?>

                </td>

                <?php
                $crudqueuename="sqs-crud-".getQueuePrefix($envName)."_".$tenantId;
                ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$crudqueuename <br>");
                    echo '</pre>'; ?>
                    <?php $matchqueuename="sqs-match-".getQueuePrefix($envName)."_".$tenantId;?>
                    <?php echo '<pre>';
                    print_r("$matchqueuename <br>");
                    echo '</pre>'; ?>

                </td>

                <?php
                $queuePrefix=getQueuePrefix($envName);
                //echo "QueuePrefix:".$queuePrefix;
                $awsRegion=getAWSRegion($queuePrefix);
                //echo "AWS RegionName:".$awsRegion;
                $crudqueuecount=send_to_sqs($crudqueuename,$awsRegion,$queuePrefix);
                ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$crudqueuecount <br>");
                    echo '</pre>'; ?>

                </td>



                <?php
                $awsRegion=getAWSRegion(getQueuePrefix($envName));
                //echo "AWS RegionName1:".$awsRegion;
                $matchqueuecount=send_to_sqs($matchqueuename,$awsRegion,$queuePrefix);
                ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$matchqueuecount <br>");
                    echo '</pre>'; ?>

                </td>

            </tr>





            <?php
        }
    }
    ?>

</table>


<?php include('Footer.php'); ?>
</body>
</html>