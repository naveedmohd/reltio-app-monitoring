<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Dev Environment Count</title>
</head>

<style>
    table {
        border-collapse: collapse;
        width: 70%;
    }

    th{
        text-align: left;
        padding: 10px;
    }

    td {
        text-align: center;
        padding: 2px;
    }

    tr:nth-child(even){background-color: lightyellow}

    th {
        background-color: #4CAF50;
        color: white;
    }

    .styled-select select {
        background: transparent;
        width: 400px;
        padding: 5px;
        font-size: 16px;
        line-height: 1;
        border: 1;
        border-radius: 0;
        height: 30px;
        margin-left: 100px;
        margin-top: 0px;
        scrollbar-highlight-color: #CC0000;
        background-color: floralwhite;
        align-content: center;
        -webkit-appearance: none;
    }
</style>

<body>
<?php
include ('SideBarNavigation.php');
require('GlobalAccessTokenExt.php');
require('TenantStatisticsByEnvironmentExt.php');
$envName="";
$authToken="";
$totalEntityCount="";
?>

<?php

$dbhost = 'localhost:3306';
$dbuser = 'root';
$dbpass = 'Admin$786';
$dbname='monitoring';

    try {
        $conn = new PDO("mysql:host=$dbhost;dbname=monitoring", $dbuser, $dbpass);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        //echo "Connected successfully";
    }
    catch(PDOException $e)
    {
        echo "Connection failed: " . $e->getMessage();
    }


    try{
        $sql_stmt="DELETE FROM DEV_ENV_COUNT_VIOLATIONS";
        $conn->exec($sql_stmt);
    }catch(PDOException $e){
        echo $sql_stmt . "<br>" . $e->getMessage();
    }
?>

<?php
if(isset($_GET["environment"])){
    $envName=$_GET["environment"];
    //echo "select environment is => ".$envName;
}
?>

<table id="dataTable" align="center" style="margin-top:50px; margin-left:300px;" border="0">
    <form id="InternalEnvironments">
        <tr align="center">
            <td align="center">
                <div class="styled-select">
                    <select name="environment"  onchange="this.form.submit()">
                        <option selected="selected" >Choose an environment from this list</option>
                        <?php

                        $environments = array(
                            "dev.reltio.com"
                        );
                        // Iterating through the environment array
                        $selected = isset($_REQUEST['environment']) ? $_REQUEST['environment'] : '';
                        foreach($environments as $item){
                            $item = htmlspecialchars($item);
                            ?>
                            <?php
                            echo '<option value="'. $item .'"'.(($item==$selected)?' selected':'').'>'. $item .'</option>';
                        }
                        ?>
                    </select>
                </div>
            </td>

        </tr>
    </form>


</table>
<tr></tr>
<tr></tr>



<table id="EntityStatistics" align="left"  border="1" style="margin-top:50px; margin-left:300px; background-color: #F0FFFF" >




    <tr align="center" bgcolor="#00ffff" >
        <td ><b>TenantId</b></td>
        <td ><b>Entity Count</b></td>
        <td ><b>Tenant Count Violation</b></td>
    </tr>


    <!-- Get AuthToken -->
    <?php

    if ($envName != null && $authToken==null) {
        $authResponse = getAuthToken($envName);
        //echo "Here is response code:".$authResponse;
        $authToken = json_decode($authResponse);
        $authToken = $authToken->{'access_token'};
        //echo $authToken;


        //Get List of AllTenantIDs for a given environment.
        try{
            $response = getTenantStatistics($envName, $authToken);
            //echo $response;
        }catch(Exception $e){
            if (substr_count($e->getMessage(), 'invalid') > 0) {
                $authResponse = getAuthToken($envName);
                $authToken = json_decode($authResponse);
                $authToken = $authToken->{'access_token'};
            }
        }

        $resArr = array();
        $resArr = json_decode($response);
        //echo $resArr;



        foreach ($resArr as $key=>$tenantId){
            ?>


            <tr align="center" >
                <td>
                    <?php echo '<pre>';print_r("$tenantId <br>"); echo '</pre>';?>

                </td>


                <?php
                $entityCount = getEntityCountForTenant($envName,$authToken,$tenantId);
                $totalEntityCount=json_decode($entityCount,true);
                //echo $entityCount
                ?>

                <td>
                    <?php echo '<pre>';
                    print_r("$entityCount <br>");
                    echo '</pre>'; ?>

                </td>


                <td>
                    <?php echo '<pre>';
                    //print_r($totalEntityCount['total']);
                    if($totalEntityCount['total']>250000){
                        //print_r("Entity Count is Violated");
                        ?>
                        <img src="../img/cancelimage.jpeg" align="center">
                    <?php
                    }else{
                        //print_r("Entity Count is OK");
                        ?>
                        <img src="../img/okimage.png" align="center">
                     <?php
                    }
                    echo '</pre>'; ?>

                </td>

            </tr>

            <?php

                try {
                    $sql_stmt = "INSERT INTO DEV_ENV_COUNT_VIOLATIONS (TENANT_ID,ENTITY_COUNT,IS_COUNT_VIOLATED)
                    VALUES ('" . $tenantId . "', '" . $entityCount . "', " . $totalEntityCount['total'] . ")";
                    //echo($sql_stmt);
                    $conn->exec($sql_stmt);
                }catch(PDOException $e){
                        //echo $sql_stmt . "<br>" . $e->getMessage();
                 }
        }
    }
    ?>



</table>


<?php include('Footer.php'); ?>
</body>
</html>