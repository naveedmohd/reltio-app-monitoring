<?php

function getTenantStatistics($envName, $accessToken){

    $configs = include('readConfig.php');

    $authURL="";
    $userName="";
    $password="";
    $url="";

    $prefix_auth_url="ENV_".$envName;
    $authURL = $configs->$prefix_auth_url;
    //echo $authURL;

    $userName = $configs->EXT_AUTH_USER;
    $password = $configs->AUTH_PASSWORD;

    $data = array(
        'username' => $userName,
        'password' => $password
    );

    $payload = json_encode($data);

    $apiURL = "https://".$envName . "/reltio" . "/tenants";
    //echo $apiURL;

    $ch = curl_init($apiURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POST, false);

    // Set HTTP Header for POST request
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' .$accessToken)
            );

    // Submit the POST request
    $result = curl_exec($ch);
    //echo $result;

    //get the error and response code
    $errors = curl_error($ch);
    $response = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    // Close cURL session handle
    curl_close($ch);

    return $result;
}


function getEntityCountForTenant($envName,$authToken,$tenantId){
    $apiURL = "https://".$envName . "/reltio/api/" . $tenantId."/entities/_total";
    //echo $apiURL;

    $ch = curl_init($apiURL);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLINFO_HEADER_OUT, true);
    curl_setopt($ch, CURLOPT_POST, false);

    // Set HTTP Header for POST request
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Authorization: Bearer ' .$authToken)
    );

    // Submit the POST request
    try {
        $result = curl_exec($ch);
    }catch(Exception $e){
        if (substr_count($e->getMessage(), 'not defined in a system') > 0) {
            $result = "Tenant :".$tenantId." does not exist.";
            echo "What is the result:".$result;
            return $result;
        }
    }

    if (substr_count($result, 'not defined in a system') > 0) {
        $result = "Tenant :".$tenantId." not defined in a system.";
    }elseif (substr_count($result, 'Forbidden') > 0){
        $result = "Access to the tenant :".$tenantId." is denied.";
    }

    //echo $result;
    curl_close($ch);
    return $result;
}

?>