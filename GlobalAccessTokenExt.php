<?php

function getAuthToken($envName) {
    $configs = include('readConfig.php');

        $authURL="";
        $userName="";
        $password="";
        $url="";
        $prefix_auth_url="ENV_".$envName;
        //echo $prefix_auth_url;

        $authURL = $configs->$prefix_auth_url;
        $userName = $configs->EXT_AUTH_USER;
        $password = $configs->AUTH_PASSWORD;

        $data = array(
            'username' => $userName,
            'password' => $password
        );

        $payload = json_encode($data);
        //echo $authURL;

        //$authURL='https://auth-stg.reltio.com/oauth/token?username=mohammad.naveed&password=Bismillah$786&grant_type=password';
        $authURL = $authURL . "?username=" . $userName . "&password=" . $password . "&grant_type=password";

        //echo $authURL;
        // Prepare new cURL resource
        $ch = curl_init($authURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        // Set HTTP Header for POST request
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Authorization: Basic cmVsdGlvX3VpOm1ha2l0YQ==',
                'Content-Length: ' . strlen($payload))
        );

        // Submit the POST request
        $result = curl_exec($ch);
        //echo $result;
        // Close cURL session handle
        curl_close($ch);

return $result;
}

?>