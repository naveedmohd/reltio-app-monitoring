<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <style>
        body {font-family: "Lato", sans-serif;}

        .sidebar {
            height: 100%;
            width: 280px;
            position: fixed;
            z-index: 1;
            top: 0;
            left: 0;
            background-color: #99CCFF;
            overflow-x: hidden;
            padding-top: 50px;
        }

        .sidebar a {
            padding: 6px 8px 6px 16px;
            text-decoration: none;
            font-size: 15px;
            width: 180px;
            color: #000000;
            background-color: #99CCFF;
            display: block;
        }

        .sidebar a:hover {
            color: #CC0000;
        }

        .main {
            margin-left: 160px; /* Same as the width of the sidenav */
            padding: 0px 10px;
        }

        @media screen and (max-height: 450px) {
            .sidebar {padding-top: 14px;}
            .sidebar a {font-size: 14px;}
        }
    </style>
</head>
<body>

<div class="sidebar">
    <table border="0"  style="margin-left:10px;">
        <tr>
            <td>
                <img src="../img/reltio.png" align="center" alt="Reltio">
            </td>
        </tr>
    </table>

    <table border="0"  style="margin-left:10px;">
        <tr>
            <td >
                <img src="../img/home.jpeg" align="left">
            </td>
            <td align="left">
                <a href="Environments.php" align="left"> Homes</a>
            </td>
        </tr>
    </table>
    <table border="0"  style="margin-left:10px;">
        <tr>
            <td >
               <img src="../img/customer.jpeg" align="left">
            </td>
            <td >
                <a href="Environments.php" align="left"> Queue Statistics:{CUST}</a>
            </td>
        </tr>
    </table>

    <table border="0"  style="margin-left:10px; " >

        <tr>
            <td >
                <img src="../img/internalcustomer.jpeg" align="left">
            </td >
            <td >
                <a href="InternalEnvironments.php" align="left"> Queue Statistics:{INT}</a>
            </td>
        </tr>
    </table>

    <table border="0"  style="margin-left:10px; " >

        <tr>
            <td >
                <img src="../img/countimage.jpeg" align="left">
            </td>
            <td >
                <a href="DevCountViolations.php" align="left"> Dev Env Count Violations</a>
            </td>
        </tr>

    </table>
    <table border="0"  style="margin-left:10px;">
        <tr>
            <td >
                <img src="../img/export.png" align="left">
            </td>
            <td align="left">
                <a href="ExportStatisticsCust.php" align="left">Export Statistics:{CUST}</a>

            </td>
        </tr>
    </table>

    <table border="0"  style="margin-left:10px;">
        <tr>
            <td >
                <img src="../img/export.png" align="left">
            </td>
            <td align="left">
                <a href="ExportStatisticsInt.php" align="left">Export Statistics:{INT}</a>

            </td>
        </tr>
    </table>

    <table border="0"  style="margin-left:10px;">
        <tr>
            <td >
                <img src="../img/reindex.png" align="left">
            </td>
            <td align="left">
                <a href="ReindexStatisticsCust.php" align="left">Reindex Statistics:{CUST}</a>

            </td>
        </tr>
    </table>

    <table border="0"  style="margin-left:10px;">
        <tr>
            <td >
                <img src="../img/reindex.png" align="left">
            </td>
            <td align="left">
                <a href="ReindexStatisticsInt.php" align="left">Reindex Statistics"{INT}</a>

            </td>
        </tr>
    </table>

</div>

<div class="main">
    <p> </p>
</div>

</body>
</html>
